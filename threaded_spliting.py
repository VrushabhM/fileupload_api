#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar  2 12:15:17 2018

@author: vrushabh
"""

from pdfminer.pdfdocument import PDFDocument
from pdfminer.pdfparser import PDFParser
from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.pdfdevice import PDFDevice, TagExtractor
from pdfminer.pdfpage import PDFPage
from pdfminer.converter import XMLConverter, HTMLConverter, TextConverter
from pdfminer.cmapdb import CMapDB
from pdfminer.layout import LAParams
from pdfminer.image import ImageWriter

#from cStringIO import StringIO
#from StringIO import StringIO
from datetime import datetime
from io import BytesIO
import re,os,getopt,sys
from PyPDF2 import PdfFileWriter, PdfFileReader
import pickle
import gevent
from gevent.threadpool import ThreadPool
from gevent.fileobject import FileObjectThread
import time
#pages_found = []
#pages_path = ''
#file_name1 = ''
#sep=''
 
def get_text(infile, page = 0,LA_analysis=False):
    output = BytesIO()
    #output = StringIO()
    manager = PDFResourceManager()
    if LA_analysis:
        converter = TextConverter(manager, output)#, laparams=LAParams())
    else:
        converter = TextConverter(manager, output)
        
    interpreter = PDFPageInterpreter(manager, converter)
    
    for page in PDFPage.get_pages(infile, set([page])):
        interpreter.process_page(page)
    converter.close()
    text = output.getvalue()
    output.close()
    return text 



#def save_pages2(path,inputfile,pages):
#      inputpdf = PdfFileReader(open(inputfile, "rb"))
#
#      #output = PdfFileWriter()
#      temp = []
#      for page in pages:
#          output = PdfFileWriter()
#          #out_file = path+'\\'+f_name.split('.pdf')[0]+'_page_'+str(page+1)+'.pdf'
#          out_file = os.path.join(path,'page_'+str(page+1)+'.pdf')#os.path.join(path,f_name.split('.pdf')[0]+'_page_'+str(page+1)+'.pdf')
#          output.addPage(inputpdf.getPage(page))
#          with open(out_file, "wb") as outputStream:
#                output.write(outputStream)
#          temp.append(out_file)
#
#      return temp
  
def count_match_kw(keywords,text):
    """count number of keyword mathched in text"""
    
    pattern = '('+'|'.join(keywords)+')'
    match = re.findall(pattern,text,re.IGNORECASE)
    l = list(set([i.lower() for i in match]))
    
    match_count = 0
    for i in keywords:
        if l.count(i):
            match_count += 1

    return match_count


def is_balance_sheet(text):
    
    BS_keywords = ['balance sheet', "shareholders  fund", 'non current liabilities',
                 'current liabilities', 'assets', 'non current assets', 'current assets']

    t = re.findall(r'\w+',text)
    text = ' '.join(t)                 
    match_count = count_match_kw(BS_keywords,text)
    if match_count == len(BS_keywords)-1:
        return True


def is_PnL(text):
    PnL_keywords = ['profit and loss','revenue','other income','total revenue',
                    'expenses','finance costs','before tax','tax expense']

    match_count = count_match_kw(PnL_keywords,text)                 
    if match_count >= len(PnL_keywords)-1:
        return True
    

def split_pages(page_l):
    #global pages_path,pages_found,file_name1,sep
    page = page_l[0]
    infile = open(page, 'rb')
    file_name = page_l[1]
    pages_path = page_l[2]

    #f_thred = FileObjectThread(infile,'rb')
    if page.count('\\'):
        sep = '\\'
    else:
        sep = '/'
    
    text = get_text(infile)
    #text = text.replace('&','and')
    text = text.decode('utf-8',errors='ignore')
    text = re.sub(r'[^\w]',' ',text)

    p = page.split(sep)[-1]
    #page_no = int(re.findall('\d+',p)[0])
    if is_balance_sheet(text):
        print("BS found in "+ str(page))
        #;page_no = int(re.findall('\d+',p)[0])
        #p = p.replace(str(page_no),str(page_no+1))
        splited_file = 'BS_'+file_name.split(sep)[-1].split('.pdf')[0]+'_'+p
   
        out_file = os.path.join(pages_path,splited_file)
        infile.close()
        os.rename(page,out_file)
        #pages_found.append(out_file)
        

    elif is_PnL(text):
        print("PnL found in "+ str(page))
        #p = page.split(sep)[-1]#;page_no = int(re.findall('\d+',p)[0])
        #p = p.replace(str(page_no),str(page_no+1))
        splited_file = 'PnL_'+file_name.split(sep)[-1].split('.pdf')[0]+'_'+p
   
        out_file = os.path.join(pages_path,splited_file)
        infile.close()
        os.rename(page,out_file)
        
        #pages_found.append(out_file)
    else:
          #pass
          infile.close()
          #return page
          os.remove(page)

    
            

def make_dir(in_file,out_file):
    dir_name = in_file.split('.pdf')[0]
    print(dir_name)
    if not os.path.isdir(dir_name):
        os.mkdir(in_file.split('.pdf')[0])
        splited_folder = os.path.join(dir_name,'splited_pages')
        os.mkdir(splited_folder)
        return splited_folder
    else:
        print('Directory already exists')
        sys.exit()

    
#splited_file = file_name.split(sep)[-1].split('.pdf')[0]+'_'+p
def save_pages2(path,inputfile,pages_path,file_name,pages):
      inputpdf = PdfFileReader(open(inputfile, "rb"))

      #output = PdfFileWriter()
      temp = []
      for page in pages:
          output = PdfFileWriter()
          #out_file = path+'\\'+f_name.split('.pdf')[0]+'_page_'+str(page+1)+'.pdf'
          out_file = os.path.join(path,'page_'+str(page+1)+'.pdf')#os.path.join(path,f_name.split('.pdf')[0]+'_page_'+str(page+1)+'.pdf')
          output.addPage(inputpdf.getPage(page))
          with open(out_file, "wb") as outputStream:
                output.write(outputStream)
          temp.append([out_file,file_name,pages_path])

      return temp
  
    
def get_fin_pages(input_file,pool_size=10):
    pool = ThreadPool(pool_size)
    
    print("Started for "+input_file)

    if input_file.count('\\'):
        file_name = input_file.split('\\')[-1]
        sep = '\\'
    else:
        file_name = input_file.split('/')[-1]
        sep='/'

    infile1 = open(input_file, 'rb')
    total_pages = PdfFileReader(infile1).numPages
    print("Total pages "+str(total_pages))
    infile1.close()
    f_path =sep.join(input_file.split(sep)[:-1])+sep
    pages_path = make_dir(input_file,'')
    #pickle = 
    #pages_path = os.path.join(f_path,"pages")
    #os.mkdir(pages_path)
    
    remainder = total_pages%pool_size
    pages = total_pages-remainder
    p = 0
    for i in range(pool_size,pages+10,pool_size):
        page_list = save_pages2(pages_path,input_file,pages_path,file_name,range(p,i))
        p = i
        pool.map(split_pages,page_list)
        pool.kill()
#        time.sleep(1)
#        for s_file in to_remove:
#              os.remove(s_file)
        print(str(i)+" pages done out of "+str(total_pages))
        
    page_list = save_pages2(pages_path,input_file,pages_path,file_name,range(p,remainder+1))
    pool.map(split_pages,page_list)
    pool.kill()
#    time.sleep(1)
#    for s_file in to_remove:
#          os.remove(s_file)


    print(str(i+remainder)+" pages done out of "+str(total_pages))
    splited_page = [os.path.join(pages_path,page) for page in os.listdir(pages_path)]
    return f_path,pages_path,splited_page


#file_name1 = "F:\data_files\Audited_Shilpi Cables_2015.pdf"

#file_dir,splited_path,splited_files = get_fin_pages(file_name1,20)
#pool_size = 20
#pool = ThreadPool(pool_size)
#file_name1 = "/media/vrushabh/TECHNOLOGY/my_stuff/data_files/Audited_Shilpi Cables_2015.pdf"
#print("Started for "+file_name1)
#
#infile1 = open(file_name1, 'rb')
#total_pages = PdfFileReader(infile1).numPages
#print("Total pages "+str(total_pages))
#infile1.close()
#pages_found = []
#f_path =file_name1.split('.pdf')[0]
#pages_path = os.path.join(f_path,"pages")
#os.mkdir(pages_path)
#
#remainder = total_pages%pool_size
#pages = total_pages-remainder
#p = 0
#for i in range(pool_size,pages+10,pool_size):
#    page_list = save_pages2(pages_path,file_name1,range(p,i))
#    p = i
#    pool.map(split_pages,page_list)
#    pool.kill()
#    print(str(i)+" pages done")
#    
#page_list = save_pages2(pages_path,file_name1,range(p,remainder+1))
#pool.map(split_pages,page_list)
#pool.kill()
    
#gevent.wait()

#
#jobs = [gevent.spawn(split_pages,page) for page in page_list]
#gevent.joinall(jobs)
#gevent.wait()


