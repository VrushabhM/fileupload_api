from flask import Flask, jsonify, Response, abort,url_for
from flask import make_response
from flask import request
from flask import render_template
import json
import sys, os
import split_pdf
from flask_celery import make_celery
from pymongo import MongoClient
client = MongoClient()
db = client.celery

#sys.path.append('./file_parsers')

#import pdf_upload
#import wordoc_upload
import time
from flask import Flask

from celery import Celery
from tasks import read_file

app = Flask(__name__)
app.config['CELERY_BROKER_URL'] = 'redis://localhost'
app.config['CELERY_RESULT_BACKEND'] = 'mongodb://localhost:27017/'

celery = make_celery(app)
# app.config['CELERY_BROKER_URL'] = 'redis://localhost:6379/'
# app.config['CELERY_RESULT_BACKEND'] = 'redis://localhost:6379/'
# celery = Celery(app.name, broker=app.config['CELERY_BROKER_URL'])
# celery.conf.update(app.config)


@celery.task(bind=True, name="doc_upload_api.read_file")
def read_file(self, inputfile, company_name):
    print(inputfile, company_name)
    split_pdf.main(inputfile, company_name)
    task_id = read_file.request.id
    return task_id


@app.route('/')
def demo():
    #print("working")
    return Response('File upload Api')


@app.route('/get_status/<_id>')
def get_status(_id):
    data = list(db.celery_taskmeta.find({"_id": _id}))
    if data:
        d = data[0]
        status = d['status']
        return Response(status)
    else:
        return Response("PENDING")


@app.route('/upload_file', methods=["POST"])
def upload_file():
    file_name = request.args.get("file")
    company_name = request.args.get("CompanyName")
    _, file_extension = os.path.splitext(file_name)
    # call to BS and PnL Extractor
    task_id = read_file.delay(file_name, company_name)
    #split_pdf.main.delay(file_name, company_name)
    _id = str(task_id)
    return_text = 'http://127.0.0.1:5000/get_status/'+_id
    return Response(return_text)


if __name__ == '__main__':
    app.run(debug=True, threaded=True)
