# -*- coding: utf-8 -*-
"""
Created on Mon Feb 26 17:23:58 2018

@author: DM390
"""

import xml.etree.ElementTree as ET
from collections import OrderedDict
import pandas
import os
from lxml import etree
import re
import getIndex


def get_mapped_lines(texts):
      top_list = []
      for i in texts:
            top_list.append(int(i.attrib['top']))      

      mapped_dict = {}
      i = 0;k = 2    
      while k != len(top_list):
            first = top_list[i]
            third = top_list[k]
            
            if first == third:
                  middle = top_list[i+1]
                  
                  if middle != first:
                        if abs(middle-first) < 5:
                              mapped_dict[top_list[i+1]] = first
                        elif abs(middle-first) > 5:
                              mapped_dict[top_list[i+1]] = first+1
      
            i += 1;k += 1
                  
      return mapped_dict


def get_lines(texts):
      mapped_dict = get_mapped_lines(texts)

      lines = OrderedDict()
      for text_tag in texts:
            text = ' '.join([i for i in text_tag.itertext()])
            text = text.strip()
            if text:
                  top = int(text_tag.attrib['top'])
                  if top in mapped_dict.keys():
                        top = mapped_dict[top]

                  left = int(text_tag.attrib['left'])
                  lines.setdefault(top, []).append([left,text])
                  
      return lines
  
    
def get_value_cords(lines,group_list):
      temp_list = []
      for key,value in lines.items():
            for val in value:
                  l = []
                  for i in group_list:
                        l.append([abs(val[0]-i),key,i,val[1]])
                  temp_list.append(min(l)[1:])
      
      return temp_list
  

def get_index(texts):
    text_list = []

    for text_tag in texts:
          text = ' '.join([i for i in text_tag.itertext()])
          text = text.strip()
          text_list.append(text)
    pattern = r'as per our|significant accounting'  
    for ind,s in enumerate(text_list):
        if re.search(pattern,s,re.IGNORECASE):
            return ind
    


def get_table(lines,temp_list,group_list):
      index = list(lines.keys())
      index.sort()
      index2 = [i for i in range(len(index))]
      df = pandas.DataFrame(index = index2,columns=group_list)
      
      mapped_list = [k for k in zip(index,[i for i in range(len(index))])]
      mapped_index = {}
      for ind in mapped_list:
            mapped_index[ind[0]] = ind[1]

      for value in temp_list:
            ind = mapped_index[value[0]]
            col = value[1]
            cell_value = value[2]
            df.iloc[ind][col] = cell_value

      return df

def get_details(df,excel_path,last_index):

    temp_df = df.copy()#[last_index+1:]#; temp_df = temp_df.applymap(str)
    #temp_df = temp_df.applymap(lambda x:str(x))
    temp_df.fillna('',inplace=True)
    temp = []
    for col in temp_df.columns:
        temp.extend(list(temp_df[col]))

    #temp = [i for i in temp] #str(i).decode('ascii',errors='ignore').encode('ascii')
    
    keywords = {
    'Chartered Accountants':['chartered accountants','chartered accounts'],
    'Director':['m.d.','managing director','executive director','director','directors','managing director & ceo','director - works'],
    'Partner':['partner'],
    'Company Secretary':['secretary','company secretary','company sec'],
    'Chief Financial Officer':['cfo','chief financial officer'],
    'Chairman':['chairman'],
    'Vice President':['vice president – finance']
    }

    temp = [i.replace('(','').replace(')','') for i in temp]
    main_list = []#;l = []
    for key,values in keywords.items():
        l = []
        for index,value in enumerate(temp):
            if values.count(value.lower()):
                #if re.match()
                #print(key,temp[index-1])
                l.extend([key,temp[index-1]])
                if re.search(r'\d{1,5}',temp[index+1]):
                    #print(temp[index+1])
                    l.append(temp[index+1])
                else:
                    continue
                #main_list.append(l)
        main_list.append(l)


    #out_path = excel_path.split('.xlsx')[0]+'_OtherDetails'+'.xlsx'
    df = pandas.DataFrame(main_list)
    #df.to_excel(out_path)
    return df

   
xml_file = r"/media/vrushabh/TECHNOLOGY/my_stuff/Updated/bs_pnlExtract/files/BS_Shilpi Cable Annual Report 2017_page_74.xml"
xml_file = r'/media/vrushabh/TECHNOLOGY/my_stuff/data_files/Mangalore Chemicals 2017/splited_pages/PnL_Mangalore Chemicals 2017_page_49.xml'
xml_file = r'/media/vrushabh/TECHNOLOGY/my_stuff/data_files/NCC Annual Report2014_15/splited_pages/BS_NCC Annual Report2014_15_page_62.xml'
with open(xml_file,'rb') as fd:
     xml_string = fd.read()

xml_parser = etree.XMLParser(recover=True)
root = etree.fromstring(xml_string, parser=xml_parser)
#root = ET.fromstring(xml_string,parser)
texts = root.findall('page/text')


index = get_index(texts)
cord_dict = OrderedDict()
for text_tag in texts[index:]:
      top = int(text_tag.attrib['top'])
      left = int(text_tag.attrib['left'])
      cord_dict.setdefault(top, []).append(left)

     
max_length = len(max(cord_dict.values(),key=len))

temp_list = []
for value in cord_dict.values():
      if len(value) == max_length:
            temp_list.append(value)

n = len(temp_list)
l = [sum(i)/n for i in zip(*temp_list)]      
#group_list = [approximate_num(i) for i in l] 
group_list = [int(i) for i in l]

group_list = list(set(group_list))
group_list.sort()

lines = get_lines(texts[index:])
cell_cords = get_value_cords(lines,group_list)
main_df = get_table(lines,cell_cords,group_list)

#
#
