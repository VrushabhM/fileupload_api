# -*- coding: utf-8 -*-
"""
Created on Sat Mar 31 11:50:26 2018

@author: DM390
"""

import requests
import time

status = 'Pending'
while status=="Pending":
    url = 'http://127.0.0.1:5000/get_status/ce0f026b-5444-47da-b642-bbd27c1499af'
    res = requests.get(url)

    if res.status_code == 200:
        status = res.text

    print(status)
    time.sleep(0.3)

